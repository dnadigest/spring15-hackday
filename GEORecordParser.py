#    Copyright 2015 DNADigest
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
#    limitations under the License.

import urllib2
from BeautifulSoup import BeautifulSoup
import re
import csv

stub = 'http://www.ncbi.nlm.nih.gov/sites/GDSbrowser?acc='
test = 'GDS5023'
pmidRegex = re.compile(r"""<a\stitle="Link\sto\sPubMed\srecord"\shref="http:\/\/www\.ncbi\.nlm\.nih\.gov\/pubmed\/.+?">""")
titleRegex = re.compile(r"""<span\sclass="title">.+?</span>""")




def getRecord(theID = test):
	recordpage = stub + theID
	record = urllib2.urlopen(recordpage).read()
	return record

def extractPMIDs(thePage):
	pmids = re.findall(pmidRegex, thePage)
	try:
		pmids = pmids[0]
		pmids = pmids.replace("<a title=\"Link to PubMed record\" href=\"http://www.ncbi.nlm.nih.gov/pubmed/", "").replace("\">", "")
		return pmids
	except:
		return 'not found'	

def extractTitle(thePage):
	titles = re.findall(titleRegex, thePage)
	try:
		titles = titles[0]
		titles = titles.replace("<span class=\"title\">", "").replace("</span>", "")
		return titles
	except:
		return 'not found'

filename = "accessions"
GEOIDs = list(csv.reader(open(filename, 'rU' ), delimiter='\n'))
datawriter = csv.writer(open('titles_pmids_short_new.csv', 'wb'), delimiter = '\t')
#print GEOIDs
outstring = ''
counter = 0;
for geoid in GEOIDs:
	geoid = ''.join(geoid)
	
	record = getRecord(geoid)
	if extractTitle(record) != 'not found':
		title = extractTitle(record)
		pmid = extractPMIDs(record)
		
		datawriter.writerow([geoid, title, pmid])
		print str(counter) + ',' + geoid + ',' + title + ',' + pmid + '\n'
	counter +=1

#test = getRecord()
#print extractPMIDs(test)
#print extractTitle(test)