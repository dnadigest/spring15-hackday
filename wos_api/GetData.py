#    Copyright 2015 DNADigest
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
#    limitations under the License.

#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
from suds.client import Client
from suds.transport.http import HttpTransport
import urllib.request
 
class HTTPSudsPreprocessor(urllib.request.BaseHandler):
    def __init__(self, SID):
        self.SID = SID
 
    def http_request(self, req):
        req.add_header('cookie', 'SID="'+self.SID+'"')
        return req
 
    https_request = http_request
 
 
class WokmwsSoapClient():
    """
    main steps you have to do:
        soap = WokmwsSoapClient()
        results = soap.search(...)
    """
    def __init__(self):
        self.url = self.client = {}
        self.SID = ''
 
        self.url['auth'] = 'http://search.isiknowledge.com/esti/wokmws/ws/WOKMWSAuthenticate?wsdl'
        self.url['search'] = 'http://search.isiknowledge.com/esti/wokmws/ws/WokSearchLite?wsdl'
 
        self.prepare()
 
    def __del__(self):
        self.close()
 
    def prepare(self):
        """does all the initialization we need for a request"""
        self.initAuthClient()
        self.authenticate()
        self.initSearchClient()
 
    def initAuthClient(self):
        self.client['auth'] = Client(self.url['auth'])
 
    def initSearchClient(self):
        http = HttpTransport()
        opener = urllib.request.build_opener(HTTPSudsPreprocessor(self.SID))
        http.urlopener = opener
        self.client['search'] = Client(self.url['search'], transport = http)
 
    def authenticate(self):
        self.SID = self.client['auth'].service.authenticate()
 
    def close(self):
        self.client['auth'].service.closeSession()
 
    def search(self, query):
        qparams = {
            'databaseID' : 'WOS',
            'userQuery' : query,
            'queryLanguage' : 'en',
            'editions' : [{
                'collection' : 'WOS',
                'edition' : 'SCI',
            },{
                'collection' : 'WOS',
                'edition' : 'SSCI',
            }]
        }
 
        rparams = {
            'count' : 5, # 1-100
            'firstRecord' : 1,
            'fields' : [{
                'name' : 'Relevance',
                'sort' : 'D',
            }],
        }
 
        return self.client['search'].service.search(qparams, rparams)