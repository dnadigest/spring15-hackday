#    Copyright 2015 DNADigest
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
#    limitations under the License.

import ftplib

### get all the urls from the ftp site:

FTP_HOST = "ftp.ncbi.nlm.nih.gov"

ftp = ftplib.FTP(FTP_HOST)
ftp.login("anonymous")
dir_listing = ftp.nlst('/geo/series/')

accessions = []
for path in dir_listing:
    accessions.extend(list(set([study.split('/')[-1] for study in ftp.nlst(path)])))

f = open('accessions','wb')
for item in accessions:
    f.write("%s\n" % item)
f.write()
